package page;

import core.BasePage;
import io.appium.java_client.TouchAction;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import java.time.Duration;
import static core.DriverFactory.getDriver;

public class CliquesPage extends BasePage {

    public void cliqueLongo(){
        cliqueLongo(By.xpath("//*[@text='Clique Longo']"));
    }

    public void cliqueDuplo(){
        WebElement e = getDriver().findElement(By.xpath("//*[@text='Clique duplo']"));
        TouchAction action = new TouchAction(getDriver());
        action.press(e).waitAction(Duration.ofMillis(50)).release().press(e).waitAction(Duration.ofMillis(50)).release().perform();
    }

    public String obterTextoCampo(){
        return getDriver().findElement(By.xpath("(//android.widget.TextView)[3]")).getText();
    }
}
