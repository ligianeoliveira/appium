package test;

import core.BaseTest;
import io.appium.java_client.MobileBy;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import page.FormularioPage;
import page.MenuPage;

import java.util.concurrent.TimeUnit;

import static core.DriverFactory.getDriver;

public class FormularioTeste extends BaseTest {

    private MenuPage menuPage = new MenuPage();
    private FormularioPage page = new FormularioPage();

    @Before
    public void inicializarAppium (){
        menuPage.acessarFormulario();
    }

    @Test
    public void devePreencherCampoTexto()  {
        page.escreverNome("Ligiane");
        Assert.assertEquals("Ligiane", page.obterNome());
    }

    @Test
    public void deveInteragirComCombo() {
        page.selecionarCombo("Nintendo Switch");
        Assert.assertEquals("Nintendo Switch", page.obterValorCombo());
    }

    @Test
    public void deveInteragirComSwitchCheckbox() {
        //Verificar status dos elementos
        Assert.assertFalse(page.isCheckMarcado());
        Assert.assertTrue(page.isSwitchMarcado());
        //Clicar nos elementos
        page.clicarCheck();
        page.clicarSwitch();
        //Verificar estados alterados
        Assert.assertTrue(page.isCheckMarcado());
        Assert.assertFalse(page.isSwitchMarcado());
    }

    @Test
    public void deveFazerCadastro() {
        //preencher os campos
        page.escreverNome("Desafio Ligiane");
        page.selecionarCombo("Nintendo Switch");
        page.clicarCheck();
        page.clicarSwitch();

        //clicar no salvar
        page.salvar();

        //verificações
        Assert.assertEquals("Nome: Desafio Ligiane", page.obterNomeCadastrado());
        Assert.assertEquals("Console: switch", page.obterConsoleCadastrado());
        Assert.assertTrue(page.obterSwitchCadastrado().endsWith("Off"));
        Assert.assertTrue(page.obterCheckCadastrado().endsWith("Marcado"));
    }

    @Test
    public void deveFazerCadastroDemorado() {
        //preencher os campos
        page.escreverNome("Teste Demorado");

        //clicar no salvar
        page.salvarDemorado();

        //verificações
        Assert.assertEquals("Nome: Teste Demorado", page.obterNomeCadastrado());
    }

    @Test
    public void deveAlterarData(){
        //interagindo com date picker
        page.clicarPorTexto("01/01/2000");
        page.clicarPorTexto("20");
        page.clicarPorTexto("OK");

        Assert.assertTrue(page.existeElementoPorTexto("20/2/2000"));
    }

    @Test
    public void deveAlterarHora(){
        //interagindo com time picker
        page.clicarPorTexto("06:00");
        page.clicar(MobileBy.AccessibilityId("10"));
        page.clicar(MobileBy.AccessibilityId("40"));
        page.clicarPorTexto("OK");

        Assert.assertTrue(page.existeElementoPorTexto("10:40"));
    }

    @Test
    public void deveInteragirComSeekbar(){
        //clicar no seekbar
        page.clicarSeekBar(0.67);
        //salvar
        page.salvar();
        //obter o valor
    }


}
