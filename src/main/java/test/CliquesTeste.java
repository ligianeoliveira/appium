package test;

import core.BaseTest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import page.CliquesPage;
import page.MenuPage;

public class CliquesTeste extends BaseTest {

    private MenuPage menu = new MenuPage();
    private CliquesPage page = new CliquesPage();

    @Before
    public void setup(){
        menu.acessarCliques();
    }

    @Test
    public void deveRealizarCliqueLongo(){
        //clique longo
        page.cliqueLongo();
        //verificar texto
        Assert.assertEquals("Clique Longo", page.obterTextoCampo());
    }

    @Test
    public void deveRealizarCliqueDuplo(){

        Assert.assertEquals("Duplo Clique", page.obterTextoCampo());
    }
}
