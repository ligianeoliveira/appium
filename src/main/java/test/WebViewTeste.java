package test;

import core.BaseTest;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import page.MenuPage;
import page.WebViewPage;

public class WebViewTeste extends BaseTest {

    private MenuPage menu = new MenuPage();
    private WebViewPage page = new WebViewPage();

    @Test
    public void deveFazerLogin(){
        //acessar menu
        menu.acessarSBHibrido();
        esperar(3000);
        page.entrarContextoWeb();
        //preencher email
        page.setEmail("ligianeoliveira90@gmail.com");
        //preencher senha
        page.setSenha("971713");
        //entrar
        page.entrar();
        //verificar mensagem sucesso
        Assert.assertEquals("Bem vindo, Ligiane Mara Oliveira!", page.getMensagem());
    }

    @After
    public void tearDown(){
        page.sairContextoWeb();
    }
}
