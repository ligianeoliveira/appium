package test;

import core.BaseTest;
import org.junit.Assert;
import org.junit.Test;
import page.AbasPage;
import page.MenuPage;

public class AbasTeste extends BaseTest {

    private MenuPage menu = new MenuPage();
    private AbasPage page = new AbasPage();

    @Test
    public void deveInteragirComAbas(){
        //acessar menu abas
        menu.acessarAbas();
        //verificar que está na aba 1
        Assert.assertTrue(page.isAba1());
        //acessar aba 2
        page.selecionarAba2();
        //verificar que está na aba 2
        Assert.assertTrue(page.isAba2());
    }
}
