package test;

import core.BaseTest;
import org.junit.Assert;
import org.junit.Test;
import page.MenuPage;
import page.SplashPage;

public class SplashTeste extends BaseTest {

    private MenuPage menu = new MenuPage();
    private SplashPage page = new SplashPage();

    //VERIFICAR ERRO NO ASSERT
    @Test
    public void deveAguardarSplashSumir(){
        //acessar menu splash
        menu.acessarSplash();
        //verificar que o splash está sendo exibido
        page.isTelaSplashVisivel();
        //aguardar saída do splash
        page.aguardarSplashSumir();
        //verificar que o formulário está aparecendo
        Assert.assertTrue(page.existeElementoPorTexto("Formulário"));
    }
}
