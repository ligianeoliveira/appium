package test;

import core.BaseTest;
import org.junit.Assert;
import org.junit.Test;
import page.MenuPage;

public class SwipeTeste extends BaseTest {

    private MenuPage menu = new MenuPage();

    @Test
    public void deveRealizarSwipe(){
        //acessar menu
        menu.acessarSwipe();
        //verificar texto 'a esquerda'
        Assert.assertTrue(menu.existeElementoPorTexto("a esquerda"));
        //swipe para a direita
        menu.swipeRight();
        //verificar texto 'E veja se'
        Assert.assertTrue(menu.existeElementoPorTexto("E veja se"));
        //clicar botão direita
        menu.clicarPorTexto("›");
        esperar(1000);
        //verificar o texto 'Chegar até o fim'
        Assert.assertTrue(menu.existeElementoPorTexto("Chegar até o fim!"));
        //swipe esquerda
        menu.swipeLeft();
        //clicar botao esquerda
        menu.clicarPorTexto("‹");
        //verificar texto 'a esquerda'
        Assert.assertTrue(menu.existeElementoPorTexto("a esquerda"));
    }


}
