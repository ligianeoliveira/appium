package test;

import core.BaseTest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import page.MenuPage;
import pageSeuBarriga.*;

public class SBTeste extends BaseTest {

    private MenuPage menu = new MenuPage();
    private SBLoginPage page = new SBLoginPage();
    private SBMenuPage menuAPP = new SBMenuPage();
    private SBContasPage contas = new SBContasPage();
    private SBMovimentacaoPage mov = new SBMovimentacaoPage();
    private SBHomePage home = new SBHomePage();
    private SBResumoPage resumo = new SBResumoPage();

    @Before
    public void setup(){
        menu.acessarSBNativo();
        page.setEmail("ligi@tst.com");
        page.setSenha("9717");
        page.entrar();
    }

    @Test
    public void deveInserirContaComSucesso(){
        //entrar em contas
        menuAPP.acessarContas();
        //digitar nome conta
        contas.setContas("Conta de Teste");
        //salvar
        contas.salvar();
        //Verificar mensagem
        Assert.assertTrue(contas.existeElementoPorTexto("Conta adicionada com sucesso"));
    }

    @Test
    public void deveExcluirConta(){
        //entrar em contas
        menuAPP.acessarContas();
        esperar(1000);
        //clique longo na conta
        contas.selecionarConta("Conta 1");
        //excluir
        contas.excluir();
        //verificar mensagem
        Assert.assertTrue(contas.existeElementoPorTexto("Conta excluída com sucesso"));
    }

    @Test
    public void deveValidarInclusaoMov(){
        //clicar no menu Mov...
        menuAPP.acessarMovimentacoes();
        esperar(1000);
        //validar descr
        mov.salvar();
        Assert.assertTrue(mov.existeElementoPorTexto("Descrição é um campo obrigatório"));

        //validar inte
        mov.setDescricao("Descricao");
        mov.salvar();
        Assert.assertTrue(mov.existeElementoPorTexto("Interessado é um campo obrigatório"));

        //validar valor
        mov.setInteressado("Ligiane");
        mov.salvar();
        Assert.assertTrue(mov.existeElementoPorTexto("Valor é um campo obrigatório"));

        //validar conta
        mov.setValor("9717");
        mov.salvar();
        Assert.assertTrue(mov.existeElementoPorTexto("Conta é um campo obrigatório"));

        //inserir com sucesso
        mov.setConta("Conta para alterar");
        mov.salvar();
        Assert.assertTrue(mov.existeElementoPorTexto("Movimentação cadastrada com sucesso"));

    }

    @Test
    public void deveAtualizarSaldoAoExcluirMovimentacao(){
        //verificar saldo da "Conta para saldo" = 534.00
        Assert.assertEquals("534.00", home.obterSaldoConta("Conta para saldo"));
        //acessar resumo
        menuAPP.acessarResumo();
        //excluir Movimentacao 3
        resumo.excluirMovimentacao("Movimentacao 3, calculo saldo");
        //validar mensagem "Movimentação removida com sucesso"
        Assert.assertTrue(resumo.existeElementoPorTexto("Movimentação removida com sucesso!"));
        //voltar home
        menuAPP.acessarHome();
        //atualizar saldo
        esperar(1000);
        home.scroll(0.2, 0.9);
        //verificar o saldo = -1000.00
        Assert.assertEquals("-1000.00", home.obterSaldoConta("Conta para saldo"));
    }
}
