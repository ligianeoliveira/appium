package test;

import core.BaseTest;
import org.junit.Assert;
import org.junit.Test;
import page.AccordionPage;
import page.MenuPage;

public class AccordionTeste extends BaseTest {

    private MenuPage menu = new MenuPage();
    private AccordionPage page = new AccordionPage();

    @Test
    public void deveInteragirComAccordion() throws InterruptedException {
        //acessar o menu
        menu.acessarAccordion();
        //clicar op 1
        page.selecionarOp1();
        //verificar texto da op 1
        Thread.sleep(1000);
        Assert.assertEquals("Esta é a descrição da opção 1", page.obterValorOp1());
    }
}
