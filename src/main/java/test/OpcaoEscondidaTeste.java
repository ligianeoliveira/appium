package test;

import core.BaseTest;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import page.MenuPage;

import static core.DriverFactory.getDriver;

public class OpcaoEscondidaTeste extends BaseTest {

    private MenuPage menu = new MenuPage();

    @Test
    public void deveEncontrarOpcaoEscondida(){
        //scroll down
        WebDriverWait wait = new WebDriverWait(getDriver(), 10);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@text='Formulário']")));
        menu.scrollDown();
        //clicar menu
        menu.clicarPorTexto("Opção bem escondida");
        //verificar mensagem
        Assert.assertEquals("Você achou essa opção", menu.obterMensagemAlerta());
        //sair
        menu.clicarPorTexto("OK");
    }
}
