package core;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class DriverFactory {

    private static AndroidDriver<MobileElement> driver;

    public static AndroidDriver<MobileElement> getDriver(){
        if(driver==null){
            createDriver();
            //createTestObjectDriver();
        }
        return driver;
    }

    private static void createDriver()  {
        DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
        desiredCapabilities.setCapability("platformName", "Android");
        desiredCapabilities.setCapability("deviceName", "emulator-5554");
        desiredCapabilities.setCapability("AutomationName", "uiautomator2");
        desiredCapabilities.setCapability(MobileCapabilityType.APP, "C:/Fontes/Aulas e Treinamentos QA/aulas-e-treinamentos-qa/CursoAppium/src/main/resources/CTAppium-1-1.apk");
        //desiredCapabilities.setCapability(MobileCapabilityType.APP, "/Users/ligianemaraoliveira/IdeaProjects/aulas-e-treinamentos-qa/CursoAppium/src/main/resources/CTAppium-1-1.apk");

        try {
            driver = new AndroidDriver<MobileElement>(new URL("http://localhost:4723/wd/hub"), desiredCapabilities);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }

    /*
    //configuração pra teste na nuvem
    private static void createTestObjectDriver()  {
        DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
        desiredCapabilities.setCapability("platformName", "Android");
        desiredCapabilities.setCapability("testobject_api_key", "B5B2B27361AE47DE98F485B7035159E6");
        desiredCapabilities.setCapability("appiumVersion", "1.7.2");
        desiredCapabilities.setCapability("AutomationName", "uiautomator2");

        try {
            driver = new AndroidDriver<MobileElement>(new URL("https://us1.appium.testobject.com/wd/hub"), desiredCapabilities);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }
    */

    public static void killDriver(){
        if(driver != null){
            driver.quit();
            driver=null;
        }
    }
}
