package pageSeuBarriga;

import core.BasePage;
import org.openqa.selenium.By;

public class SBContasPage extends BasePage {

    public void setContas(String nome){ escrever(By.className("android.widget.EditText"), nome); }

    public void salvar(){ clicarPorTexto("SALVAR"); }

    public void selecionarConta(String conta){ cliqueLongo(By.xpath("//*[@text='"+conta+"']")); }

    public void excluir(){ clicarPorTexto("EXCLUIR"); }
}
